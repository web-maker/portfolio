$(document).ready(function(){
	tinymce.init({
		selector: ".editor",
		width: 700,
		language: "ru",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	});

	$('.collapse-slide').on('show', function (){
		$(this).next('.show-hidden-panel').addClass('hide');
	});
	$('.collapse-slide').on('hide', function (){
		$(this).next('.show-hidden-panel').removeClass('hide');
	});
	
});