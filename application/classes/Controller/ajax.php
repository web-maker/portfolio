<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller{

  public $static_pages_num = 5;
  public $works_num = 5;
  public $categories_num = 5;
  public $stats_num = 20;

	public function before()
	{
    if(!Request::initial()->is_ajax())
    {
      $this->redirect('/');
    }
	}  
  
  private function _delete($id = false){
    $action = $this->request->action();
    switch($action){
      case "delete_static":
        $model = new Model_Static();
        break;
      case "delete_work":
        $model = new Model_Works();
        break;
      case "delete_category":
        $model = new Model_Categories();
        break;
      case "delete_stats":
        $model = new Model_Stats();
        break;
    }
    if($model->delete($id)){
      return true;
    }else{
      return false;
    }
  }
  
  public function action_add_static(){
    $post = $this->request->post();

    $validation = Validation::factory($post)->rule('title', 'not_empty')
                                            ->rule('description', 'not_empty')
                                            ->rule('alias', 'not_empty')
                                            ->rule('page_title', 'not_empty');
    if($validation->check()){
    
      $alias_check = DB::select(array(DB::expr('COUNT(alias)'), 'total'))
          ->from('static')
          ->where('alias', '=', $post['alias'])
          ->execute()
          ->get('total');
      
      if($alias_check == 0){
        $static_model = new Model_Static();
        if($static_model->save($post)){
          $result['status'] = "done";
          $result['messages'] = array("Страница успешно добавлена");
        }else{
          $result['status'] = "fail";
          $result['messages'] = array("Произошла ошибка. Не удалось добавить страницу.");
        }
      }else{
        $result['status'] = "fail";
        $result['messages'] = array("Значение поля alias должно быть уникальным.");
      }
      
    }else{
      $result['status'] = "fail";
      $result['messages'] = $validation->errors('comments');
    }
    echo json_encode($result);
  }
  
  public function action_add_work(){
    $post = $this->request->post();
    
    $validation = Validation::factory($post)->rule('item_name', 'not_empty')
                                            ->rule('item_description', 'not_empty');
    if($validation->check()){
      $works_model = new Model_Works();
      
      if($post['category'] == 0 || $post['category'] == '') $post['category'] = null;
      
      if($works_model->save($post)){
        $result['status'] = "done";
        $result['messages'] = array("Работа успешно добавлена");
      }else{
        $result['status'] = "fail";
        $result['messages'] = array("Произошла ошибка. Не удалось добавить работу.");
        return false;
      }
      
      if(!empty($post['images'])){
        $images_model = new Model_Images();
        $images_model->save($post['images'], mysql_insert_id());
      }

    }else{
      $result['status'] = "fail";
      $result['messages'] = $validation->errors('comments');
    }
    echo json_encode($result);
  }
  
  public function action_update_static(){
    $post = $this->request->post();
    $validation = Validation::factory($post)->rule('title', 'not_empty')
                                            ->rule('description', 'not_empty')
                                            ->rule('alias', 'not_empty')
                                            ->rule('page_title', 'not_empty');
    if($validation->check()){
    
      $alias_check = DB::select(array(DB::expr('COUNT(alias)'), 'total'))
          ->from('static')
          ->where('alias', '=', $post['alias'])
          ->execute()
          ->get('total');
      if($alias_check == 0){
        $static_model = new Model_Static();
        if($static_model->update($post)){
          $result['status'] = "done";
          $result['messages'] = array("Страница успешно обновлена");
        }else{
          $result['status'] = "fail";
          $result['messages'] = array("Произошла ошибка. Не удалось обновить страницу.");
        }
      }else{
        $result['status'] = "fail";
        $result['messages'] = array("Значение поля alias должно быть уникальным.");
      }
    }else{
      $result['status'] = "fail";
      $result['messages'] = $validation->errors('comments');
    }
    echo json_encode($result);
  }

  public function action_update_work(){
    $post = $this->request->post();
    $validation = Validation::factory($post)->rule('item_name', 'not_empty')
                                            ->rule('item_description', 'not_empty');

    if($validation->check()){
      $work_model = new Model_Works();
      
      if($post['category'] == 0 || $post['category'] == '') $post['category'] = null;
      
      if($work_model->update($post)){
        $result['status'] = "done";
        $result['messages'] = array("Страница успешно обновлена");
      }else{
        $result['status'] = "fail";
        $result['messages'] = array("Произошла ошибка. Не удалось обновить страницу.");
      }
      
      if(!empty($post['images'])){
        $images_model = new Model_Images();
        $images_model->update($post['images'], $post['id']);
      }
    }else{
      $result['status'] = "fail";
      $result['messages'] = $validation->errors('comments');
    }
    echo json_encode($result);
  }
  
  public function action_remove_image(){
    $post = $this->request->post();
    Request::factory('image/delete')->post('image_name', $post['image_name'])->execute();
  }
  
  public function action_get_all_pages(){
    $static_model = new Model_Static();
    $list_items = $static_model->get_all_pages($this->static_pages_num, 0);
    $pagination = Helper_Pagination::pagination($static_model->count_all(), $this->static_pages_num, 1, 'admin/static');//helper get required parameters
    
    echo View::factory('templates/admin/static/list')->set('list_items', $list_items)
                                                     ->set('pagination', $pagination)->render();
  }
  
  public function action_get_all_works(){
    $works_model = new Model_Works();
    $list_items = $works_model->get_all_works($this->works_num, 0);
    $pagination = Helper_Pagination::pagination($works_model->count_all(), $this->works_num, 1, 'admin/works');//helper get required parameters
    
    echo View::factory('templates/admin/works/list')->set('list_items', $list_items)
                                                    ->set('pagination', $pagination)->render();
  }
  
  public function action_get_all_categories(){
    $categories_model = new Model_Categories();
    $list_items = $categories_model->get_all_categories($this->categories_num, 0);
    $pagination = Helper_Pagination::pagination($categories_model->count_all(), $this->categories_num, 1, 'admin/categories');//helper get required parameters
    
    echo View::factory('templates/admin/categories/list')->set('list_items', $list_items)
                                                    ->set('pagination', $pagination)->render();
  }
  
  public function action_get_stats(){
    $stats_model = new Model_Stats();
    $list_items = $stats_model->get_all_stats($this->stats_num, 0);
    $pagination = Helper_Pagination::pagination($stats_model->count_all(), $this->stats_num, 0, 'admin/stats');//helper get required parameters
    
    echo View::factory('templates/admin/stats/list')->set('list_items', $list_items)
                                                    ->set('pagination', $pagination)->render();
  }
  
  public function action_get_page(){
    $static_model = new Model_Static();
    $result = $static_model->get_page($this->request->param('id'));
    $page = $result[0];
    echo json_encode($page);
  }
  
  public function action_get_work(){
    $work_model = new Model_Works();
    $images_model = new Model_Images();
    $result = $work_model->get_work($this->request->param('id'));
    $work = $result[0];
    
    $images = $images_model->get_images($this->request->param('id'));
    if(!empty($images)){
      $work['images'] = $images;
    }
    echo json_encode($work);
  }
  
  public function action_get_category(){
    $categories_model = new Model_Categories();
    $result = $categories_model->get_category($this->request->param('id'));
    $category = $result[0];
    
    echo json_encode($category);
  }
  
  public function action_get_works(){
    $works_model = new Model_Works();
    $images_model = new Model_Images();
    $works = $works_model->get_all_works(2, $this->request->param('id')-1);
    foreach($works as $key => $work){
      $image = $images_model->get_image_by_name($work['preview']);
      if(!empty($image)){
        $works[$key]['preview_desc'] = $image[0]['description'];
      }
    }
    
    if(!empty($works)){
      echo View::factory('templates/themes/default/works_list', array('works' => $works))->render();
    }else{
      echo 0;
    }
  }  
  
  public function action_delete_static(){
    if(self::_delete($this->request->param('id'))){
      $result['status'] = "done";
      $result['messages'] = array("Страница успешно удалена");
    }else{
      $result['status'] = "fail";
      $result['messages'] = array("Произошла ошибка. Не удалось удалить страницу.");
    }
    echo json_encode($result);
  }
  
  public function action_delete_stats(){
    if(self::_delete()){
      $result['status'] = "done";
      $result['messages'] = array("Статистика успешно очищена");
    }else{
      $result['status'] = "fail";
      $result['messages'] = array("Произошла ошибка. Не удалось очистить статистику");
    }
    echo json_encode($result);
  }
  
  public function action_delete_work(){
    if(self::_delete($this->request->param('id'))){
      $result['status'] = "done";
      $result['messages'] = array("Работа успешно удалена");
    }else{
      $result['status'] = "fail";
      $result['messages'] = array("Произошла ошибка. Не удалось удалить работу.");
    }
    echo json_encode($result);
  }
  
  public function action_update_settings(){
    $post = $this->request->post();
    $fail = 0;
    foreach($post as $item => $value){
      $site_config = Kohana::$config->load('site');
      //$site_config->set($item, $value);
      if(!$site_config->set($item, $value)){
        $fail = 1;
        break;
      }
    }
    if($fail == 0){
      $result['status'] = "done";
      $result['messages'] = array("Настройки успешно изменены");
    }else{
      $result['status'] = "fail";
      $result['messages'] = array("Произошла ошибка. Не удалось изменить настройки.");
    }
    echo json_encode($result);
  }
  
  public function action_add_category(){
    $post = $this->request->post();
    $validation = Validation::factory($post)->rule('title', 'not_empty');
    if($validation->check()){
      $categories_model = new Model_Categories();
      if($categories_model->save($post)){
        $result['status'] = "done";
        $result['messages'] = array("Категория успешно добавлена");
      }else{
        $result['status'] = "fail";
        $result['messages'] = array("Произошла ошибка. Не удалось добавить категорию.");
        return false;
      }
    }else{
      $result['status'] = "fail";
      $result['messages'] = $validation->errors('comments');
    }
    echo json_encode($result);
  }
  
  public function action_update_category(){
    $post = $this->request->post();
    $validation = Validation::factory($post)->rule('title', 'not_empty');
    
    if($validation->check()){
      $categories_model = new Model_Categories();
      if($categories_model->update($post)){
        $result['status'] = "done";
        $result['messages'] = array("Категория успешно обновлена");
      }else{
        $result['status'] = "fail";
        $result['messages'] = array("Произошла ошибка. Не удалось обновить категорию.");
      }
    }else{
      $result['status'] = "fail";
      $result['messages'] = $validation->errors('comments');
    }
    echo json_encode($result);
  }
  
  public function action_delete_category(){
    if(self::_delete($this->request->param('id'))){
      $result['status'] = "done";
      $result['messages'] = array("Категория успешно удалена");
    }else{
      $result['status'] = "fail";
      $result['messages'] = array("Произошла ошибка. Не удалось удалить категорию.");
    }
    echo json_encode($result);
  }

} // End Controller_Ajax