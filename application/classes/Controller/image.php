<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Image extends Controller{
  
  //TODO change path to statics
  
  private static $images_dir;
  private static $thumbs_dir;

  public function before(){
    self::$images_dir = realpath(DOCROOT.'uploads/images/');
    self::$thumbs_dir = realpath(DOCROOT.'uploads/thumbs/');
    /*if(Request::initial() != HTTP_Request::POST)
    {
      $this->redirect('/');
    }*/
  }

  public function action_index(){
    
  }
  
  public function action_upload(){
  
    $view = View::factory('templates/admin/image');
    $error_message = NULL;
    $filename = NULL;
    $img_type = NULL;

    if($this->request->method() == Request::POST){
      if(isset($_FILES['image'])){
        $filename = $this->_save_image($_FILES['image']);
      }
    }
    
    if(!$filename){
      $error_message = "Error!";
    }else{
      $response = '<script type="text/javascript">';
      $response .= 'var img_name = "'.$filename.'";';
      $response .= 'var img_desc = "";';
      $response .= 'window.parent.addImage(img_name, img_desc);';
      $response .= '</script>';
      $view->response = $response;
    }
   
    $view->error = $error_message;
    $this->response->body($view);
  }
  
  public function action_delete()     //delete image
  {
    $post = $this->request->post();
    $filename = realpath(self::$images_dir.'/'.$post['image_name'].'.jpg');
    $thumbnail = realpath(self::$thumbs_dir.'/'.$post['image_name'].'_thumb.jpg');
    try{
      unlink($filename);
      unlink($thumbnail);
      $images_model = new Model_Images();
      $images_model->delete($post['image_name']);
      $result['status'] = "done";
      $result['messages'] = array("Изображение успешно удалено");
    }catch(Exception $e){
      $result['status'] = "fail";
      $result['messages'] = array($e->getMessage());
    }
    echo json_encode($result);
  }
  
  public function action_resize($image){
  
  }
  
  public function action_crop($image){
  
  }
  
  public function action_get_image(){
    $image = $this->request->param('image');
    $width = (int)$this->request->param('width');
    $height = (int)$this->request->param('height');
    
    $rendered = false;
    
    if($image){
      $filename = realpath(DOCROOT.'uploads/images/'.$image.'.jpg');
      if(is_file($filename)){
        $this->_render_image($filename, $width, $height);
        $rendered = true;
      }else{
        echo 'No file!';
      }
    }else{
      echo 'Empty query!';
    }
    
    if(!$rendered){
      $this->response->status(404);
    }
  }

  protected function _save_image($image){
    if(
      !Upload::valid($image) OR
      !Upload::not_empty($image) OR
      !Upload::type($image, array('jpg', 'jpeg', 'png', 'gif'))
    ){
      return false;
    }
    if(!self::$images_dir){              //TODO check directory
      mkdir(self::$images_dir, 0644);
    }
    chmod(self::$images_dir, 0644);
    chmod(self::$thumbs_dir, 0644);
    if($file = Upload::save($image, NULL, self::$images_dir)){
      $filename = uniqid();
      Image::factory($file)->save(self::$images_dir.'/'.$filename.'.jpg');//main img
      Image::factory($file)->resize(200, 200)->save(self::$thumbs_dir.'/'.$filename.'_thumb.jpg');
        
      unlink($file);
      return $filename;
    }
    return false;
  }
  
  protected function _render_image($filename, $width = NULL, $height = NULL){
    // Calculate ETag from original file padded with the dimension specs
    $etag_sum = md5(base64_encode(file_get_contents($filename)).$width.','.$height);
    $this->response->headers('Content-Type', 'image/jpeg')
                   ->headers('Cache-Control', 'max-age = '.Date::HOUR.', public, must-revalidate')
                   ->headers('Expires', gmdate('D, d M Y H:i:s', time() + Date::HOUR).' GMT')
                   ->headers('Last-Modified', date('r', filemtime($filename)))
                   ->headers('ETag', $etag_sum);
                   
    if($this->request->headers('if-none-match') AND (string)$this->request->headers('if-none-match') === $etag_sum){
      $this->response->status(304)->headers('Content-Length', '0');
    }else{
      $result = Image::factory($filename);
      if(!empty($width)){
        $result->resize($width, $height, Image::NONE)->render('jpg');
      }
      $this->response->body($result);
    }
  }

} // End Controller_Image