<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Site_Works extends Controller_Site_Common{

	public function before()
	{
    parent::before();
    $this->title = 'Сайт';
	}

	public function action_index()
	{
    $id = $this->request->param('work');
    
    $works_model = new Model_Works();
    $images_model = new Model_Images();
    
    if(isset($id))
    {
      $work = $works_model->get_work($id);
      $work = $work[0];
      $this->title .= ' - '.$work['title'];
      $this->description = $work['meta_description'];
      
      $work['images'] = $images_model->get_images($id);
      
      $this->template->content = View::factory('templates/themes/default/work_page', array('work' => $work));
    }else{
      $works = $works_model->select_all();
      $this->template->content = View::factory('templates/themes/default/works_all', array('works' => $works));//complete it
    }

	}
} // End Controller_Works_Static
