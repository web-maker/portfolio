<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Site_Main extends Controller_Site_Common{

	public function before()
	{
    parent::before();
    /*$this->title = 'Сайт';
    $this->description = 'Сайт';*/
    $this->title = Kohana::$config->load('site')->get('title');
    $this->description = Kohana::$config->load('site')->get('description');
	}
  
  private static function add_stats(){
    
    $stats_model = new Model_Stats;
    $user_agent = Request::$user_agent;
    
    $browser = get_browser($user_agent, true);
    $browser['version'] = 'v.'.$browser['version'];
    
    $stats_data = array(
      'date' => date('d.m.y - G:i:s'),
      'ip' => Request::$client_ip,
      'user_agent' => $browser['browser'].' '.$browser['version'],
      'os' => $browser['platform']
    );
    
    $stats_model->save($stats_data);
  }

	public function action_index()
	{
    $this->template->content = View::factory('templates/themes/default/works');
	}
  
  public function action_feedback()
  {
    $this->template->content = View::factory('templates/themes/default/feedback');
  }
  
  public function after(){
    parent::after();
    
    if(Cookie::get('user') != 1){
      Cookie::set('user', '1', 3600*6);//6 hours
      self::add_stats();
    }

  }
} // End Site_Main
