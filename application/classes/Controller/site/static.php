<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Site_Static extends Controller_Site_Common{

	public function before()
	{
    parent::before();
    $this->title = 'Сайт';
	}

	public function action_index()
	{
    $alias = $this->request->param('page');
    
    $pages_model = new Model_Static();
    
    if(isset($alias))
    {
      $page = $pages_model->get_page_by_alias($alias);
      $page = $page[0];
      $this->title .= ' - '.$page['title'];
      $this->description = $page['meta_description'];
      $this->template->content = View::factory('templates/themes/default/static', array('page' => $page));
    }else{
      $pages = $pages_model->select_all();
      $this->template->content = View::factory('templates/themes/default/static_all', array('pages' => $pages));
    }

	}
} // End Controller_Site_Static
