<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Site_Common extends Controller_Template{
	public $template = 'templates/themes/default/index';
	public $title;
	public $description;

	protected $auth, $user;
	
	public function before()
	{
		//Auth
		$this->auth = Auth::instance();
		$this->user = $this->auth->get_user();

		parent::before();
	}

	public function after()
	{
		View::set_global(array(
			'title' => !isset($this->title) ? 'Default title' : $this->title,
			'description' => !isset($this->description) ? 'Default description' : $this->description,
			));

		parent::after();
	}
} // End Site_Common