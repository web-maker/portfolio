<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Login extends Controller_Admin_Common{

	public function action_index()
	{
		if($this->auth->logged_in('admin')){
			$this->redirect('admin');
		}else{
			if($_POST)
			{
				$post = Arr::extract($_POST, array('username', 'password', 'remember'));
				$login = $this->auth->login($post['username'], $post['password'], $post['remember']);
				if($login){
					$this->redirect('admin');
				}else{
					echo "Ошибка авторизации";
				}
			}
		}

		$this->title = 'Авторизация';
		$this->description = 'Авторизация в панели управления';
		$this->template->content = View::factory('templates/admin/login');
	}

	// создание тестового админа
	public function action_createuser() {
		$title  = '';
		$content = '';
		$this->template->bind('title', $title)->bind('content', $content);
		
		try {
			$users = new Model_User();
			$us = array('username' => 'admin', 'password' => 'admin', 'password_confirm' => 'admin', 'email' => 'test@admin.com');
			$users->create_user($us, array('username','password','email'));
			$role = ORM::factory('role')->where('name', '=', 'login')->find();
			$users->add('roles', $role);
			$role = ORM::factory('role')->where('name', '=', 'admin')->find();
			$users->add('roles', $role);
		}
		catch (ORM_Validation_Exception $e)
		{
			 echo debug::vars($e->errors('validation'));
		}
		
		exit();
	}
	
	public function action_logout()
	{
		$this->auth->logout();
		$this->redirect('admin/login');
	}

} // End Controller_Admin_Login