<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Orders extends Controller_Admin_Panel{

	public function before()
	{
		parent::before();
    $this->title .= ' - '.'Заказы';
	}

	public function action_index()
	{
    $data = "Список заказов";
    $this->template->content->set('section', View::factory('templates/admin/orders/index')->set('data', 'Список заказов'));
	}
	public function action_delete()
	{

	}
} // End Controller_Admin_Orders
