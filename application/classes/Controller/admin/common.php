<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Admin_Common extends Controller_Template{
	public $template = 'templates/admin/index';
	public $title;
	public $description;

	protected $auth, $user;
	
	public function before()
	{
		//Auth
		$this->auth = Auth::instance();
		$this->user = $this->auth->get_user();

		parent::before();
	}

	public function after()
	{
		View::set_global(array(
			'title' => !isset($this->title) ? 'Дефолтный заголовок' : $this->title,
			'description' => !isset($this->description) ? 'Дефолтное описание' : $this->description,
			));

		parent::after();
	}
} // End Controller_Admin_Common