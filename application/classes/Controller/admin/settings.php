<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Admin_Settings extends Controller_Admin_Panel{

	public function before()
	{
		parent::before();
    $this->title .= ' - '.'Настройки';
	}

	public function action_index()
	{
    $site_config = Kohana::$config->load('site');
    $this->template->content->set('section', View::factory('templates/admin/settings/index')->set('site_config', $site_config));
	}
} // End Controller_Admin_Settings