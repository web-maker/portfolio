<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Categories extends Controller_Admin_Panel{

  public $items_num = 5;

	public function before()
	{
		parent::before();
    $this->title .= ' - '.'Категории';
	}

	public function action_index()
	{
    $post = $this->request->post();
    $get = (array)$this->request->query();
    $categories_model = new Model_Categories();

    $page = isset($get['page']) ? (int)$get['page'] : 1;
    $list_items = $categories_model->get_all_categories($this->items_num, $page-1);

    $pagination = Helper_Pagination::pagination($categories_model->count_all(), $this->items_num, $page, $this->request->uri());//helper get required parameters

    $this->template->content->set('section', View::factory('templates/admin/categories/index', array('data' => $post))
                            ->set('list', View::factory('templates/admin/categories/list')->set('list_items', $list_items)
                                                                                      ->set('pagination', $pagination)));
	}

} // End Controller_Admin_Categories