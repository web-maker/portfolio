<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Static extends Controller_Admin_Panel{

  public $pages_num = 5;

	public function before()
	{
		parent::before();
    $this->title .= ' - '.'Статические страницы';
	}

	public function action_index()
	{
    $post = $this->request->post();
    $get = (array)$this->request->query();
    $static_model = new Model_Static();
    $i18n = Kohana::$config->load('i18n')->as_array();
    $data = "Статические страницы";

    $page = isset($get['page']) ? (int)$get['page'] : 1;
    $list_items = $static_model->get_all_pages($this->pages_num, $page-1);

    $pagination = Helper_Pagination::pagination($static_model->count_all(), $this->pages_num, $page, $this->request->uri());//helper get required parameters

    $this->template->content->set('section', View::factory('templates/admin/static/index', array('data' => $post, 'langs' => $i18n))
                            ->set('list', View::factory('templates/admin/static/list')->set('list_items', $list_items)
                                                                                      ->set('pagination', $pagination)));
	}

} // End Controller_Admin_Static