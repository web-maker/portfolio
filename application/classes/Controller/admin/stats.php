<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Stats extends Controller_Admin_Panel{

  public $stats_num = 20;

	public function before()
	{
		parent::before();
    $this->title .= ' - '.'Статистика';
	}

	public function action_index()
	{
    $get = (array)$this->request->query();
    $stats_model = new Model_Stats();
    $data = "Статистика";
    $page = isset($get['page']) ? (int)$get['page'] : 1;
    $list_items = $stats_model->get_all_stats($this->stats_num, $page-1);
    
    $pagination = Helper_Pagination::pagination($stats_model->count_all(), $this->stats_num, $page, $this->request->uri());//helper get required parameters
    
    $this->template->content->set('section', View::factory('templates/admin/stats/index')
                            ->set('list', View::factory('templates/admin/stats/list')->set('list_items', $list_items)
                                                                                     ->set('pagination', $pagination)));
	}
} // End Controller_Admin_Stats