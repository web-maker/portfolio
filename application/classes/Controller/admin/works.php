<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Works extends Controller_Admin_Panel{

  public $works_num = 5;

	public function before()
	{
		parent::before();
    $this->title .= ' - '.'Работы';
	}

	public function action_index()
	{
    $post = $this->request->post();
    $get = (array)$this->request->query();
    $works_model = new Model_Works();
    $categories_model = new Model_Categories();
    $i18n = Kohana::$config->load('i18n')->as_array();
    $data = "Список работ";

    $page = isset($get['page']) ? (int)$get['page'] : 1;
    $list_items = $works_model->get_all_works($this->works_num, $page-1);
    
    $categories = $categories_model->select_all();
    
    $pagination = Helper_Pagination::pagination($works_model->count_all(), $this->works_num, $page, $this->request->uri());//helper get required parameters

    $this->template->content->set('section', View::factory('templates/admin/works/index', array('data' => $post, 'categories' => $categories))
                            ->set('list', View::factory('templates/admin/works/list')->set('list_items', $list_items)
                                                                                      ->set('pagination', $pagination)));

	}

} // End Controller_Admin_Works