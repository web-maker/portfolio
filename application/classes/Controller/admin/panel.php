<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Panel extends Controller_Admin_Common{

	public function before()
	{
		parent::before();
    if($this->auth->logged_in('admin')){
      $this->title = 'Админка';
      $this->template->content = View::factory('templates/admin/main')->set('username', $this->user->username);
    }else{
      $this->redirect('admin/login');
    }
	}

	public function action_index()
	{
    $this->title .= ' - '.'Главная страница панели управления';
    $this->template->content->set('section', View::factory('templates/admin/main_page')
                            ->set('username', $this->user->username));
	}
} // End Controller_Admin_Panel
