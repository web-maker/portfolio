<?php defined('SYSPATH') or die('No direct script access.');

/***Works Images***/

class Model_Images extends Model{

  //$images - array of works images

  protected $_tableImages = 'images';
  
  public function save($images, $work_id)
  {
    if(!is_array($images)){
      $images = array_chunk(explode(',', $images), 2);
    }
    
    foreach($images as $key => $image){
      $query = DB::insert($this->_tableImages, array('name', 'description', 'work_id'))
          ->values(array($image[0], $image[1], $work_id))->execute();
    }
  }
  
  public function update($images, $work_id)
  {
    if(!is_array($images)){
      $images = array_chunk(explode(',', $images), 2);
    }
    
    foreach($images as $key => $image){
      $image_id = DB::select('id')->from($this->_tableImages)->where('name', '=', $image[0])->execute()->as_array();
      
      if(empty($image_id)){
        $query = DB::insert($this->_tableImages, array('name', 'description', 'work_id'))
                    ->values(array($image[0], $image[1], $work_id))->execute();
      }else{
        $query = DB::update($this->_tableImages)->set(array(
                                                                'description' => $image[1]
                                                              ))->where('name', '=', $image[0])->execute();
      }
    }
  }  
  
  public function get_images($id)
  {
    $work = DB::select()->from($this->_tableImages)->where('work_id', '=', $id)->execute()->as_array();
		return $work;
  }
  
  public function get_image_by_name($name)
  {
    $work = DB::select()->from($this->_tableImages)->where('name', '=', $name)->execute()->as_array();
		return $work;
  }  
  
  public function delete($name){
    $query = DB::delete($this->_tableImages)->where('name', '=', $name);
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
} // End Model_Works