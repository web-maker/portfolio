<?php defined('SYSPATH') or die('No direct script access.');

/***Categories Model***/

class Model_Categories extends Model{

  protected $_tableCategories = 'categories';
  
  public function save($post)
  {
    $query = DB::insert($this->_tableCategories, array('title', 'description'))
        ->values(array($post['title'], $post['description']));
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
  public function update($post)
  {
    $query = DB::update($this->_tableCategories)->set(array(
                                                    'title' => $post['title'],
                                                    'description' => $post['description']
                                                  ))->where('id', '=', $post['id']);
    if($query->execute() == 1 || $query->execute() == 0){
      return true;
    }else{
      return false;
    }
  }    
  
  public function delete($id){
    $query = DB::delete($this->_tableCategories)->where('id', '=', $id);
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
	public function get_all_categories($items_num, $start = 0)
	{
    $categories = DB::select()->from($this->_tableCategories)->limit($items_num)->offset($items_num * $start)->order_by('id', 'DESC')->execute()->as_array();
		return $categories;
	}
  
	public function get_category($id)
	{
    $category = DB::select()->from($this->_tableCategories)->where('id', '=', $id)->execute()->as_array();
		return $category;
	}
  
  public function count_all(){
    $total_categories = DB::select('id')->from($this->_tableCategories)->execute()->count();
    return $total_categories;
  }
  
  public function select_all()
  {
    $categories = DB::select()->from($this->_tableCategories)->order_by('id' ,'DESC')->execute()->as_array();
    if($categories)
    {
      return $categories;
    }else{
      return array();
    }
  }
  
} // End Model_Categories