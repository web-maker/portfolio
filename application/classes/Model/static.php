<?php defined('SYSPATH') or die('No direct script access.');

/***Static Pages Model***/

class Model_Static extends Model{
  
  protected $_tableStatic = 'static';
  
  public function save($post)
  {
    $query = DB::insert($this->_tableStatic, array('title', 'description', 'alias', 'page_title', 'page_body'))
        ->values(array($post['title'], $post['description'], $post['alias'], $post['page_title'], $post['page_body']));
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
  public function update($post)
  {
    $query = DB::update($this->_tableStatic)->set(array(
                                                    'title' => $post['title'],
                                                    'description' => $post['description'],
                                                    'alias' => $post['alias'],
                                                    'page_title' => $post['page_title'],
                                                    'page_body' => $post['page_body']
                                                  ))->where('id', '=', $post['id']);
    if($query->execute() == 1 || $query->execute() == 0){
      return true;
    }else{
      return false;
    }
  }  
  
  public function delete($id){
    $query = DB::delete('static')->where('id', '=', $id);
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
	public function get_all_pages($items_num, $start = 0)
	{
    $static_pages = DB::select()->from($this->_tableStatic)->limit($items_num)->offset($items_num * $start)->order_by('id', 'DESC')->execute()->as_array();
		return $static_pages;
	}
  
	public function get_page($id)
	{
    $page = DB::select()->from($this->_tableStatic)->where('id', '=', $id)->execute()->as_array();
		return $page;
	}
  
  public function get_page_by_alias($alias)
  {
    $page = DB::select()->from($this->_tableStatic)->where('alias', '=', $alias)->execute()->as_array();
    return $page;
  }
  
  public function count_all(){
    $total_pages = DB::select('id')->from($this->_tableStatic)->execute()->count();
    return $total_pages;
  }
  
  public function select_all()
  {
    $result = DB::select()->from($this->_tableStatic)->order_by('id' ,'DESC')->execute()->as_array();
    if($result)
    {
      return $result;
    }else{
      return array();
    }
  }
  
} // End Model_Static