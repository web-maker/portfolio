<?php defined('SYSPATH') or die('No direct script access.');

/***Works Model***/

class Model_Works extends Model{

  protected $_tableWorks = 'works';
  
  public function save($post)
  {
    $date = date('d.m.Y');
    $query = DB::insert($this->_tableWorks, array('title', 'meta_description', 'item_name', 'item_description', 'category', 'preview', 'date'))
        ->values(array($post['title'], $post['meta_description'], $post['item_name'], $post['item_description'], $post['category'], $post['preview'], $date));
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
  public function update($post)
  {
    $query = DB::update($this->_tableWorks)->set(array(
                                                    'title' => $post['title'],
                                                    'meta_description' => $post['meta_description'],
                                                    'item_name' => $post['item_name'],
                                                    'item_description' => $post['item_description'],
                                                    'category' => $post['category'],
                                                    'preview' => $post['preview']
                                                  ))->where('id', '=', $post['id']);
    if($query->execute() == 1 || $query->execute() == 0){
      return true;
    }else{
      return false;
    }
  }    
  
  public function delete($id){
    $query = DB::delete($this->_tableWorks)->where('id', '=', $id);
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
	public function get_all_works($items_num, $start = 0)
	{
    $works = DB::select()->from($this->_tableWorks)->limit($items_num)->offset($items_num * $start)->order_by('id', 'DESC')->execute()->as_array();
		return $works;
	}
  
	public function get_work($id)
	{
    $work = DB::select()->from($this->_tableWorks)->where('id', '=', $id)->execute()->as_array();
		return $work;
	}
  
  public function get_work_by_alias($alias)
  {
    $work = DB::select()->from($this->_tableWorks)->where('alias', '=', $alias)->execute()->as_array();
    return $work;
  }
  
  public function count_all(){
    $total_works = DB::select('id')->from($this->_tableWorks)->execute()->count();
    return $total_works;
  }
  
  public function select_all()
  {
    $works = DB::select()->from($this->_tableWorks)->order_by('id' ,'DESC')->execute()->as_array();
    if($works)
    {
      return $works;
    }else{
      return array();
    }
  }
  
} // End Model_Works