<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User {

	protected $_primary_key = 'id';
	
	protected $_table_columns = array(
		'id' => array('type' => 'int'),
		'email' => array('type' => 'string'),
		'username' => array('type' => 'string'),
		'password' => array('type' => 'string'),
		'logins' => array('type' => 'int'),
		'last_login' => array('type' => 'int')
	);

	protected $_has_many = array(
		'user_tokens' => array('model' => 'user_tokens'),
		'roles' => array('model' => 'role', 'through' => 'roles_users'),
	);

	public static function get_password_validation($values)
	{
			return Validation::factory($values)
					->rule('password', 'min_length', array(':value', 4))
					->rule('password_confirm', 'matches', array(':validation', ':field', 'password'));
	}

	/*
	public function find_username($username)
    {
        return ORM::factory('user', array('username' => $username))->loaded();
    }
	
	public function validation_auth($data)
	{
		return Validation::factory($data)
			->rule('password', 'not_empty')
			->rule('username', 'not_empty')
			->rule('username',  array($this, 'find_username'));
	}
	
	public function validation_registration($data)
	{
		return Validation::factory($data)
				->rules('password', array(
					array('not_empty'),
					array('min_length', array(':value', 4))
				))
				->rules('password_confirm', array(
					array('matches', array(':validation',':field','password')),
					array('not_empty')
				))
				->label('password_confirm', 'Повторный пароль')
				->rules('username', array(
					array('not_empty'),
					array('min_length', array(':value', 4)),
					array('max_length', array(':value', 32)),
					array(array($this, 'unique'), array('username', ':value')),
				))
				->rules('email', array(
					array('not_empty'),
					array('min_length', array(':value', 6)),
					array('max_length', array(':value', 127)),
					array('email'),
				));
	}
	
	public function validation_edit($data,$user)
	{
		$validation_user = Validation::factory($data);
		$unique = array();
		
		if($data['username'] != $user['username'])
			$validation_user->rules('username', array(array(array($this, 'unique'), array('username', ':value'))));
		
		if(isset($data['password']) && $data['password'] != '') {
			$validation_user->rules('password', array(
					array('not_empty'),
					array('min_length', array(':value', 4))
				))
				->rules('password_confirm', array(
					array('matches', array(':validation',':field','password')),
					array('not_empty')
				))->label('password_confirm', 'Повторный пароль');
		}
		
		$validation_user->rules('username', array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 32))
			))
			->rules('email', array(
				array('not_empty'),
				array('min_length', array(':value', 6)),
				array('max_length', array(':value', 127)),
				array('email'),
			));
				
		return $validation_user;
	}
	
	public static function get_password_validation($values)
	{
			return Validation::factory($values)
					->rule('password', 'min_length', array(':value', 4))
					->rule('password_confirm', 'matches', array(':validation', ':field', 'password'));
	}
	
	public function get_users($total, $start = 0)
	{
		$users = ORM::factory('user')->limit($total)->offset(($total*$start))->order_by('id', 'DESC')->find_all();
		
		foreach($users as $key => $user) {
			$role = $user->roles->where('name', '!=', 'login')->find()->as_array();
		
			$result_users[$key]['id'] =  $user->id;
			$result_users[$key]['username'] =  $user->username;
			$result_users[$key]['role'] = $role['name'];
		}
		
		return $result_users;

	}
*/
	
}