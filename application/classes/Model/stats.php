<?php defined('SYSPATH') or die('No direct script access.');

/***Stats Model***/

class Model_Stats extends Model{

  protected $_tableWorks = 'stats';
  
  public function save($stats_data)
  {
    $query = DB::insert($this->_tableWorks, array('date', 'ip', 'user_agent', 'os'))
        ->values(array($stats_data['date'], $stats_data['ip'], $stats_data['user_agent'], $stats_data['os']));
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }

  public function count_all(){
    $total_stats = DB::select('id')->from($this->_tableWorks)->execute()->count();
    return $total_stats;
  }  
  
  public function delete(){
    $query = DB::delete($this->_tableWorks);
    if($query->execute()){
      return true;
    }else{
      return false;
    }
  }
  
	public function get_all_stats($items_num, $start = 0)
	{
    $stats = DB::select()->from($this->_tableWorks)->limit($items_num)->offset($items_num * $start)->order_by('id', 'DESC')->execute()->as_array();
		return $stats;
	}
  
} // End Model_Stats