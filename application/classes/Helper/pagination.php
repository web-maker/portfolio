<?php defined('SYSPATH') or die('No direct script access.');

/*
Pagination Helper

$total_items - total static pages in database
$items_by_step - number of total pages of paginator
$active_page - current page
$uri - current controller uri

*/

class Helper_Pagination{

  public static function pagination($total_items, $items_by_step, $active_page, $uri){
    
		if ($total_items <= $items_by_step)
			return NULL;
      
		$start = 1;
		$pagination = '<ul>';
    
    if($total_items > $items_by_step)
			$number_of_pages = ceil($total_items/$items_by_step);
    
    if($active_page >= 5 && $number_of_pages > 6){
      
			$pagination .= '<li><a href="/'.$uri.'/?page=1" >1</a></li>';	
			$pagination .= '<li class="active" ><a>...</a></li>';
      
      if($active_page <= $number_of_pages - 3){
        $start = $active_page - 1;
        $visible_pages = $active_page + 1;
      }else{
        $visible_pages = $number_of_pages;
        $start = $number_of_pages - 4;
      }
      
    }else{
      if($number_of_pages > 6){
        $visible_pages = 5;
      }else{
        $visible_pages = $number_of_pages;
      }
    }

		for ($i = $start; $visible_pages  >= $i; $i++) {
      $pagination .= '<li class="'.(($active_page==$i)?'active':'').'"><a href="/'.$uri.'/?page='.$i.'" >'.$i.'</a></li>';
		}
    
    if($number_of_pages >= 7 && $active_page < $number_of_pages - 2){
			$pagination .= '<li class="active" ><a>...</a></li>';
			$pagination .= '<li><a href="/'.$uri.'/?page='.$number_of_pages.'" >'.$number_of_pages.'</a></li>';	
    }
    
    $back_link = ' <a href="/'.$uri.'/?page=1">Вернуться на светлую сторону Силы</a>';
    if($active_page > $number_of_pages)
      $pagination = "Неправильная страница. Пожалуйста, не трогайте адрес в адресной строке ;)".$back_link;
    
    return $pagination;
  }

} //End Helper Pagination