<h3>Настройки админ-панели</h3>
<hr class="title_line" />
<div id="settings_form">
  <form method="post" action="">
    <div class="field_item">
      <label>Метатег title сайта</label>
      <input type="text" class="input-xxlarge js-config_title" name="title" value="<?php echo $site_config->title; ?>" />
    </div>
    <div class="field_item">
      <label>Метатег description сайта</label>
      <input type="text" class="input-xxlarge js-config_description" name="description" value="<?php echo $site_config->description; ?>" />
    </div>
    <div class="form_action">
      <button class="js-update_btn btn btn-success" type="button">Обновить</button>
    </div>
  </form>
</div>
<script>
  var data = new Object();
  var alertTemplate = "<div class=\"alert\" data-dismiss=\"alert\">" +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                        "<div class=\"error_text\"></div>" +
                      "</div>";
  
  function ajaxStart(el){
    el.prepend('<div id="overlay">');
  }
  
  function ajaxEnd(el, result){
    var text = '';
    var item;
    $('#overlay').remove();
    if(el.children('.alert').length == 0){
      el.prepend(alertTemplate);
    }
    var alertItem = el.children('.alert');
    for(item in result.messages){
      text += '<p>' + result.messages[item] + '</p>';
    }
    if(result.status == 'done'){
      alertItem.removeClass().addClass('alert alert-success');
    }
    if(result.status == 'fail'){
      alertItem.removeClass().addClass('alert alert-error');
    }
    alertItem.addClass('show').children('.error_text').html(text);
  }
  
  $('.js-update_btn').click(function(){
    var el = $('#settings_form');
    ajaxStart(el);
    data = {
      'title' : $('.js-config_title').val(),
      'description' : $('.js-config_description').val()
    }
    $.ajax({
      type: 'POST',
      url: '/ajax/update_settings',
      data: 'title='+data.title+'&description='+data.description,
      dataType: 'json'
    }).done(function(result){
      ajaxEnd(el, result);
    });  
  });

</script>