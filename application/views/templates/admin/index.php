<!DOCTYPE html>
<html lang="ru">
<head>
  <title><?php echo $title; ?></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php echo HTML::style('bootstrap/css/bootstrap.min.css')."\r\n"; ?>
  <?php echo HTML::style('bootstrap/css/styles.css')."\r\n"; ?>
  <?php echo HTML::script('bootstrap/js/jquery-1.10.2.min.js')."\r\n"; ?>
  <?php echo HTML::script('bootstrap/js/bootstrap.min.js')."\r\n"; ?>
  <?php echo HTML::script('bootstrap/tinymce/tinymce.min.js')."\r\n"; ?>
  <?php echo HTML::script('bootstrap/js/functions.js')."\r\n"; ?>
</head>
<body>
<div class="container"><?php echo $content; ?></div>
</body>