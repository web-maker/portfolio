<h3>Статистика посещений</h3>
<hr class="title_line" />
<div class="stats_list">
  <h4>Список работ</h4>
  <div id="stats_list">
    <?php echo $list; ?>
  </div>
  <script>
  $(document).ready(function(){
  
    var alertTemplate = "<div class=\"alert\" data-dismiss=\"alert\">" +
                          "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                          "<div class=\"error_text\"></div>" +
                        "</div>";
  
    function ajaxStart(el){
      el.prepend('<div id="overlay">');
    }
    
    function ajaxEnd(el, result){
      var text = '';
      var item;
      $('#overlay').remove();
      if(el.children('.alert').length == 0){
        el.prepend(alertTemplate);
      }
      var alertItem = el.children('.alert');
      for(item in result.messages){
        text += '<p>' + result.messages[item] + '</p>';
      }
      if(result.status == 'done'){
        alertItem.removeClass().addClass('alert alert-success');
      }
      if(result.status == 'fail'){
        alertItem.removeClass().addClass('alert alert-error');
      }
      alertItem.addClass('show').children('.error_text').html(text);
    }
  
    $('#delete_stats').click(function(){
      var el = $('#stats_list');
      ajaxStart(el);
      $('#delete_stats').attr('disabled', 'disabled');
      $.ajax({
        type: 'POST',
        url: '/ajax/delete_stats',
        dataType: 'json'
      }).done(function(result){
        ajaxEnd(el, result);
        $('#delete_stats').removeAttr('disabled');
        if(result.status == 'done'){
          $('.pages_wrap .alert').remove();
          $('#stats_list').load('/ajax/get_stats');
        }
      });
    });
    
  });
  </script>
</div>