<?php if(!empty($list_items)): ?>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>id</th>
      <th>Дата посещения</th>
      <th>IP адрес</th>
      <th>Браузер пользователя</th>
      <th>Операционная система пользователя</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($list_items as $key => $item): ?>
    <tr>
      <td><?php echo $item['id']; ?></td>
      <td><?php echo $item['date']; ?></td>
      <td><?php echo $item['ip']; ?></td>
      <td><?php echo $item['user_agent']; ?></td>
      <td><?php echo $item['os']; ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<button id="delete_stats" class="btn btn-small btn-danger">Очистить статистику</button>
<?php else: ?>
  <p>На данный момент в базе пусто :(</p>
<? endif; ?>

<?php if($pagination != ''): ?>
<div class="pagination">
  <?php echo $pagination; ?>
</div>
<?php endif; ?>