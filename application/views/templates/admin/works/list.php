<?php if(!empty($list_items)): ?>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>id</th>
      <th>Имя страницы</th>
      <th>Действие</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($list_items as $key => $item): ?>
    <tr>
      <td><? echo $item['id']; ?></td>
      <td><a href="#" target="_blank"><?php echo $item['title']; ?></a></td>
      <td class="span2">
        <div class="btn-group">
          <button class="btn btn-small btn-success edit_work" data-item-num="<?php echo $item['id']; ?>">Редактировать</button>
          <button class="btn btn-small btn-danger delete_work" data-item-num="<?php echo $item['id']; ?>">Удалить</button>
        </div>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
  <p>На данный момент в базе пусто :(</p>
<?php endif; ?>

<?php if($pagination != ''): ?>
<div class="pagination">
  <?php echo $pagination; ?>
</div>
<?php endif; ?>