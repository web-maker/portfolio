<h3>Управление работами портфолио</h3>
<hr class="title_line" />
<div id="work_form" class="collapse-slide collapse">
  <div id="uploader">
    <form method="post" action="<?php echo URL::site('image/upload') ?>" id="photo_upload_form" enctype="multipart/form-data" target="hiddenframe">
      <input type="file" name="image" class="upload_input" style="display: none;" />
      <button class="btn btn-primary upload_button">Загрузить изображение</button>
    </form>
    <iframe id="hiddenframe" name="hiddenframe" style="width: 0px; height: 0px; border: 0px;"></iframe>
    <ul id="upload_result" class="thumbnails"></ul>
  </div>
  <script>
    var uploadTemplate, uploadableFile;

    var alertTemplate = "<div class=\"alert\" data-dismiss=\"alert\">" +
                          "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                          "<div class=\"error_text\"></div>" +
                        "</div>";

    function addImage(img_name, img_desc, work_preview){
    
      var description_template;
      
      if(img_desc != ''){
        description_template = '<div class="comment_wrap">' + 
                                    '<input type="text" class="image_description" placeholder="Введите описание" value="' + img_desc + '" />' +
                                   '</div>';
      }else{
        description_template = '';
      }
      
      if(work_preview != '' && img_name == work_preview){
        check_template = '<input type="checkbox" name="thumb" class="js-check_thumb" checked="checked" />';
        selected_item = ' main_prev';
      }else{
        check_template = '<input type="checkbox" name="thumb" class="js-check_thumb" />';
        selected_item = '';
      }
    
      uploadTemplate = '<li class="item span2' + selected_item +'">' +
                          check_template +
                          '<div class="thumbnail">' + '<img src="/image/get_image/' + img_name + '" data-name="' + img_name + '" class="item_img" />' + 
                            description_template +
                          '</div>' +
                          '<span class="delete_item"></span>' + 
                        '</li>';
      $('#upload_result').append(uploadTemplate);
    }
    $(document).ready(function(){
      $('#uploader .upload_button').click(function(){
        $('#work_form > .alert').remove();
        $('#uploader .upload_input').click();
      });
      $('#uploader .upload_input').change(function(){
        if($(this).val() != ''){
          $('#photo_upload_form').submit();
          $(this).val('');
          $('#uploader .alert').remove();
        }
      });
      
      $('#upload_result').on('click', '.thumbnail', function(){
        var addCommentTemplate = '<div class="comment_wrap">' + 
                                 '<input type="text" class="image_description" placeholder="Введите описание" />' +
                                 '</div>'
        if($(this).find('.comment_wrap').length > 0){
          return false;
        }else{
          $(this).append(addCommentTemplate);
        }
      });
      
      $('#upload_result').on('click', '.delete_item', function(){
        var imageName = $(this).parent().find('.item_img').attr('data-name');
        var deleteButton = $(this);
        $.ajax({
          beforeSend: function(){
            deleteButton.parent().addClass('loading');
          },
          type: 'POST',
          url: '/ajax/remove_image',
          data: 'image_name=' + imageName,
          dataType: 'json'
        }).done(function(result){
          var text = '';
          if($('#uploader').children('.alert').length == 0){
            $('#uploader').prepend(alertTemplate);
          }
          var alertItem = $('#uploader').children('.alert');
          for(item in result.messages){
            text += '<p>' + result.messages[item] + '</p>';
          }
          if(result.status == 'done'){
            alertItem.removeClass().addClass('alert alert-success');
            deleteButton.parent().remove();
          }
          if(result.status == 'fail'){
            alertItem.removeClass().addClass('alert alert-error');
          }
          alertItem.addClass('show').children('.error_text').html(text);
        });
      });
      
      $('#upload_result').on('click', '.js-check_thumb', function(){
        if($(this).parents('.item').hasClass('main_prev')){
          $(this).parents('.item').removeClass('main_prev');
        }else{
          $('#upload_result .main_prev').removeClass('main_prev').children('.js-check_thumb').removeAttr('checked');
          $(this).parents('.item').addClass('main_prev');
        }
      });
      
    });
  </script>
  <form method="post" action="">
    <div class="field_item">
      <input type="text" class="meta_title input-xxlarge" placeholder="Метатег title" name="title" value="" />
    </div>
    <div class="field_item">
      <input type="text" class="meta_description input-xxlarge" placeholder="Метатег description" name="meta_description" value="" />
    </div>
    <div class="field_item">
      <input type="text" class="item_name input-xxlarge" placeholder="Введите название работы" name="item_name" value="" />
    </div> 
    <div class="field_item">
      <label class="control-label">Выберите категорию</label>
      <select name="item_category" class="item_category">
        <option></option>
        <?php foreach($categories as $key => $category): ?>
        <option value="<?php echo $category['id']; ?>"><?php echo $category['title']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="field_item">
      <label class="control-label">Введите описание</label>
      <div class="controls input-xxlarge description_textarea">
          <textarea cols="3" rows="15" class="item_description editor" placeholder="Введите описание" name="item_description"></textarea>
      </div>
    </div>
    <div class="form_action">
      <button class="js-add_button btn btn-success" type="button">Добавить</button>
      <button class="js-edit_button btn btn-success hide" type="button">Сохранить</button>
      <button class="js-cancel_button btn btn-danger" type="button" data-toggle="collapse" data-target="#work_form">Закрыть</button>
    </div>
    <script>
    $(document).ready(function(){

      function ajaxStart(el){
        el.prepend('<div id="overlay">');
      }
      
      function ajaxEnd(el, result){
        var text = '';
        var item;
        $('#overlay').remove();
        if(el.children('.alert').length == 0){
          el.prepend(alertTemplate);
        }
        var alertItem = el.children('.alert');
        for(item in result.messages){
          text += '<p>' + result.messages[item] + '</p>';
        }
        if(result.status == 'done'){
          alertItem.removeClass().addClass('alert alert-success');
        }
        if(result.status == 'fail'){
          alertItem.removeClass().addClass('alert alert-error');
        }
        alertItem.addClass('show').children('.error_text').html(text);
      }
      
      $('.js-add_button').click(function(){
        var el = $('#work_form');
        var data, masseges = new Object();
        var description = tinyMCE.get('item_description');
        var mainPrev = '';
        
        var imagesArray = [];
        $('#upload_result .item').each(function(){
          var itemArray = [];
          var imageName = $(this).find('.item_img').attr('data-name');
          var imageDesc;
          if($(this).find('.image_description').length == 0 || $(this).find('.image_description').val() == '' ){
            imageDesc = '';
          }else{
            imageDesc = $(this).find('.image_description').val();
          }
          
          if($(this).hasClass('main_prev')){
            mainPrev = imageName;
          }
          
          itemArray.push(imageName);
          itemArray.push(imageDesc);

          imagesArray[$(this).index()] = itemArray;
        });
        
        ajaxStart(el);
        $('#add_page, #add_cancel').attr('disabled', 'disabled');
        data = {
          'title' : $('.meta_title').val(),
          'meta_description' : $('.meta_description').val(),
          'item_name' : $('.item_name').val(),
          'item_description' : description.getContent(),
          'images' : imagesArray,
          'category' : $('.item_category').val(),
          'preview' : mainPrev
        }
        $.ajax({
          type: 'POST',
          url: '/ajax/add_work',
          data: 'title='+data.title+'&meta_description='+data.meta_description+'&item_name='+data.item_name+'&item_description='+data.item_description+'&images='+data.images+'&category='+data.category+'&preview='+data.preview,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('.js-add_button, .js-cancel_button').removeAttr('disabled');
          if(result.status == 'done'){
            $('#work_form input[type=text]').val('');
            $('#upload_result').html('');
            description.setContent('');
            $('.item_category').val('');
            $('.works_wrap .alert').remove();
            $('#works_list').load('/ajax/get_all_works');
          }
        });
      });
      
      $('.js-edit_button').click(function(){
        var el = $('#work_form');
        var data, masseges = new Object();
        var description = tinyMCE.get('item_description');
        var mainPrev = '';
        
        var imagesArray = [];
        $('#upload_result .item').each(function(){
          var itemArray = [];
          var imageName = $(this).find('.item_img').attr('data-name');
          var imageDesc;
          if($(this).find('.image_description').length == 0 || $(this).find('.image_description').val() == '' ){
            imageDesc = '';
          }else{
            imageDesc = $(this).find('.image_description').val();
          }
          
          if($(this).hasClass('main_prev')){
            mainPrev = imageName;
          }          
          
          itemArray.push(imageName);
          itemArray.push(imageDesc);

          imagesArray[$(this).index()] = itemArray;
        });
        ajaxStart(el);
        $('.js-edit_button, .js-cancel_button').attr('disabled', 'disabled');
        data = {
          'id' : $('#item_id').val(),
          'title' : $('.meta_title').val(),
          'meta_description' : $('.meta_description').val(),
          'item_name' : $('.item_name').val(),
          'item_description' : description.getContent(),
          'images' : imagesArray,
          'category' : $('.item_category').val(),
          'preview' : mainPrev
        }
        $.ajax({
          type: 'POST',
          url: '/ajax/update_work',
          data: 'id='+data.id+'&title='+data.title+'&meta_description='+data.meta_description+'&item_name='+data.item_name+'&item_description='+data.item_description+'&images='+data.images+'&category='+data.category+'&preview='+data.preview,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('.js-edit_button, .js-cancel_button').removeAttr('disabled');
          $('.works_wrap .alert').remove();
          $('#works_list').load('/ajax/get_all_works');
        });
      });
      
      $('.js-cancel_button').click(function(){
        $('#upload_result').html('');
        var pageBody = tinyMCE.get('item_description');
        $('#work_form .alert').remove();
        $('#work_form input[type=text]').val('');
        pageBody.setContent('');
        $('.item_category').val('');
      });
      
      $('#works_list').on('click', '.edit_work', function(){
        $('#work_form .alert').remove();
        var id = $(this).attr('data-item-num');
        var description = tinyMCE.get('item_description');
        $.ajax({
          type: 'POST',
          url: '/ajax/get_work/' + id,
          dataType: 'json'
        }).done(function(result){
          if($('#work_form').hasClass('in') == false){
            $('.show-hidden-panel').click();
          }
          $('.js-add_button').addClass('hide');
          $('.js-edit_button').removeClass('hide');
          if($('#item_id').length > 0){
            $('#item_id').val(id);
          }else{
            $('#work_form form').prepend('<input type="hidden" id="item_id" value="' + result.id + '" />');
          }
          $('.meta_title').val(result.title);
          $('.meta_description').val(result.meta_description);
          $('.item_name').val(result.item_name);
          description.setContent(result.item_description);
          $('.item_category').val(result.category);
          $('#upload_result').html('');
          
          if(result.images != undefined){
            for(var i = 0; i < result.images.length; i++){
              addImage(result.images[i].name, result.images[i].description, result.preview);
            }
          }
          
        }); 
      });
      
      $('#works_list').on('click', '.delete_work', function(){
        var el = $('.works_wrap');
        var id = $(this).attr('data-item-num');
        ajaxStart(el);
        $.ajax({
          type: 'POST',
          url: '/ajax/delete_work/' + id,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          if(result.status == 'done'){
            $('#works_list').load('/ajax/get_all_works');
          }
        }); 
      });
      
    });
    </script>
  </form>
</div>
<button class="btn btn-primary show-hidden-panel" type="button" data-toggle="collapse" data-target="#work_form">Добавить работу</button>
<hr />
<div class="works_wrap">
  <h4>Список работ</h4>
  <div id="works_list">
    <?php echo $list; ?>
  </div>
</div>