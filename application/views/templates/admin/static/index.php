<h3>Управление статическими страницами</h3>
<hr class="title_line" />
<div id="static_form" class="collapse-slide collapse">
  <form method="post" action="">
    <div class="field_item">
      <input type="text" id="title" class="input-xxlarge" placeholder="Метатег title" name="title" value="" />
    </div>
    <div class="field_item">
      <input type="text" id="description" class="input-xxlarge" placeholder="Метатег description" name="description" value="" />
    </div>
    <div class="field_item">
      <input type="text" id="alias" class="input-xxlarge" placeholder="Введите alias" name="alias" value="" />
    </div>
    <div class="field_item">
      <input type="text" id="page_title" class="input-xxlarge" placeholder="Введите заголовок страницы" name="page_title" value="" />
    </div>
    <div class="field_item">
      <label class="control-label">Введите описание</label>
      <div class="controls input-xxlarge description_textarea">
          <textarea cols="3" rows="15" class="editor" placeholder="Введите описание" name="page_body"></textarea>
      </div>
    </div>
    <div class="form_action">
      <button class="btn btn-success" type="button" id="add_page">Добавить</button>
      <button class="btn btn-success hide" type="button" id="edit_page">Сохранить</button>
      <button class="btn btn-danger" type="button" id="add_cancel" data-toggle="collapse" data-target="#static_form">Закрыть</button>
    </div>
    <script>
    $(document).ready(function(){
    
      var alertTemplate = "<div class=\"alert\" data-dismiss=\"alert\">" +
                            "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                            "<div class=\"error_text\"></div>" +
                          "</div>";
    
      function ajaxStart(el){
        el.prepend('<div id="overlay">');
      }
      
      function ajaxEnd(el, result){
        var text = '';
        var item;
        $('#overlay').remove();
        if(el.children('.alert').length == 0){
          el.prepend(alertTemplate);
        }
        var alertItem = el.children('.alert');
        for(item in result.messages){
          text += '<p>' + result.messages[item] + '</p>';
        }
        if(result.status == 'done'){
          alertItem.removeClass().addClass('alert alert-success');
        }
        if(result.status == 'fail'){
          alertItem.removeClass().addClass('alert alert-error');
        }
        alertItem.addClass('show').children('.error_text').html(text);
      }
    
      $('#add_page').click(function(){
        var el = $('#static_form');
        var data, masseges = new Object();
        var pageBody = tinyMCE.get('page_body');
        ajaxStart(el);
        $('#add_page, #add_cancel').attr('disabled', 'disabled');
        data = {
          'title' : $('#title').val(),
          'description' : $('#description').val(),
          'alias' : $('#alias').val(),
          'page_title' : $('#page_title').val(),
          'page_body' : pageBody.getContent()
        }
        $.ajax({
          type: 'POST',
          url: '/ajax/add_static',
          data: 'title='+data.title+'&description='+data.description+'&alias='+data.alias+'&page_title='+data.page_title+'&page_body='+data.page_body,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('#add_page, #add_cancel').removeAttr('disabled');
          if(result.status == 'done'){
            $('#static_form input[type=text]').val('');
            pageBody.setContent('');
            $('.pages_wrap .alert').remove();
            $('#pages_list').load('/ajax/get_all_pages');
          }
        });
      });
      
      $('#edit_page').click(function(){
        var el = $('#static_form');
        var data, masseges = new Object();
        var pageBody = tinyMCE.get('page_body');
        ajaxStart(el);
        $('#add_page, #add_cancel').attr('disabled', 'disabled');
        data = {
          'id' : $('#page_num').val(),
          'title' : $('#title').val(),
          'description' : $('#description').val(),
          'alias' : $('#alias').val(),
          'page_title' : $('#page_title').val(),
          'page_body' : pageBody.getContent()
        }
        $.ajax({
          type: 'POST',
          url: '/ajax/update_static',
          data: 'id='+data.id+'&title='+data.title+'&description='+data.description+'&alias='+data.alias+'&page_title='+data.page_title+'&page_body='+data.page_body,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('#add_page, #add_cancel').removeAttr('disabled');
          $('.pages_wrap .alert').remove();
          $('#pages_list').load('/ajax/get_all_pages');
        });
      });
      
      $('#add_cancel').click(function(){
        if($('#add_page').hasClass('hide')){
          $('#edit_page').addClass('hide');
          $('#add_page').removeClass('hide');
          var pageBody = tinyMCE.get('page_body');
          $('#static_form .alert').remove();
          $('#static_form input[type=text]').val('');
          pageBody.setContent('');
        }
      });
      
      $('#pages_list').on('click', '.edit_page', function(){
        $('#static_form .alert').remove();
        var id = $(this).attr('data-page-num');
        var pageBody = tinyMCE.get('page_body');
        $.ajax({
          type: 'POST',
          url: '/ajax/get_page/' + id,
          dataType: 'json'
        }).done(function(result){
          if($('#static_form').hasClass('in') == false){
            $('.show-hidden-panel').click();
          }
          $('#add_page').addClass('hide');
          $('#edit_page').removeClass('hide');
          if($('#page_num').length > 0){
            $('#page_num').val(id);
          }else{
            $('#static_form form').prepend('<input type="hidden" id="page_num" value="' + result.id + '" />');
          }
          $('#title').val(result.title);
          $('#description').val(result.description);
          $('#alias').val(result.alias);
          $('#page_title').val(result.page_title);
          pageBody.setContent(result.page_body);
        }); 
      });
      
      $('#pages_list').on('click', '.delete_page', function(){
        var el = $('.pages_wrap');
        var id = $(this).attr('data-page-num');
        ajaxStart(el);
        $.ajax({
          type: 'POST',
          url: '/ajax/delete_static/' + id,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('#pages_list').load('/ajax/get_all_pages');
        }); 
      });
      
    });
    </script>
  </form>
</div>
<button class="btn btn-primary show-hidden-panel" type="button" data-toggle="collapse" data-target="#static_form">Добавить страницу</button>
<hr />
<div class="pages_wrap">
  <h4>Список страниц</h4>
  <div id="pages_list">
  <?php echo $list; ?>
  </div>
</div>