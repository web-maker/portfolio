<div class="container">
  <form class="login-form" action="/admin/login" method="post">
    <h2>Port<span class="text-error">fo</span>lio</h2>
    <div class="input-wrap">
      <input type="text" class="input-block-level" placeholder="Логин" name="username" value="<?php echo HTML::chars(Arr::get($_POST, 'username')); ?>">
      <span class="text-error"><?php if(isset($errors['username'])) echo $errors['username']; ?></span>
    </div>
    <div class="input-wrap">
      <input type="password" class="input-block-level" placeholder="Пароль" name="password" />
      <span class="text-error"><?php if(isset($errors['password'])) echo $errors['password']; ?></span>
    </div>
    <label class="checkbox">
	  <input name="remember" type="checkbox" value="1" /> Запомнить меня
    </label>
    <button class="btn btn-primary" type="submit">Войти</button>
  </form>
</div>