<h3>Управление категориями</h3>
<hr class="title_line" />
<div id="category_form" class="collapse-slide collapse">
  <script>
    var uploadTemplate, uploadableFile;

    var alertTemplate = "<div class=\"alert\" data-dismiss=\"alert\">" +
                          "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                          "<div class=\"error_text\"></div>" +
                        "</div>";
  </script>
  <form method="post" action="">
    <div class="field_item">
      <input type="text" class="item_title input-xxlarge" placeholder="Имя категории" name="item_title" value="" />
    </div>
    <div class="field_item">
      <input type="text" class="item_description input-xxlarge" placeholder="Описание категории" name="item_description" value="" />
    </div>
    <div class="form_action">
      <button class="js-add_button btn btn-success" type="button">Добавить</button>
      <button class="js-edit_button btn btn-success hide" type="button">Сохранить</button>
      <button class="js-cancel_button btn btn-danger" type="button" data-toggle="collapse" data-target="#category_form">Закрыть</button>
    </div>
    <script>
    $(document).ready(function(){

      function ajaxStart(el){
        el.prepend('<div id="overlay">');
      }
      
      function ajaxEnd(el, result){
        var text = '';
        var item;
        $('#overlay').remove();
        if(el.children('.alert').length == 0){
          el.prepend(alertTemplate);
        }
        var alertItem = el.children('.alert');
        for(item in result.messages){
          text += '<p>' + result.messages[item] + '</p>';
        }
        if(result.status == 'done'){
          alertItem.removeClass().addClass('alert alert-success');
        }
        if(result.status == 'fail'){
          alertItem.removeClass().addClass('alert alert-error');
        }
        alertItem.addClass('show').children('.error_text').html(text);
      }
      
      $('.js-add_button').click(function(){
        var el = $('#category_form');
        var data, masseges = new Object();
        
        ajaxStart(el);
        $('#add_page, #add_cancel').attr('disabled', 'disabled');
        data = {
          'title' : $('.item_title').val(),
          'description' : $('.item_description').val()
        }
        $.ajax({
          type: 'POST',
          url: '/ajax/add_category',
          data: 'title='+data.title+'&description='+data.description,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('.js-add_button, .js-cancel_button').removeAttr('disabled');
          if(result.status == 'done'){
            $('#category_form input[type=text]').val('');
            $('.categories_wrap .alert').remove();
            $('#items_list').load('/ajax/get_all_categories');
          }
        });
      });
      
      $('.js-edit_button').click(function(){
        var el = $('#category_form');
        var data, masseges = new Object();
        
        ajaxStart(el);
        $('.js-edit_button, .js-cancel_button').attr('disabled', 'disabled');
        data = {
          'id' : $('#item_id').val(),
          'title' : $('.item_title').val(),
          'description' : $('.item_description').val()
        }
        $.ajax({
          type: 'POST',
          url: '/ajax/update_category',
          data: 'id='+data.id+'&title='+data.title+'&description='+data.description,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          $('.js-edit_button, .js-cancel_button').removeAttr('disabled');
          $('.categories_wrap .alert').remove();
          $('#items_list').load('/ajax/get_all_categories');
        });
      });
      
      $('.js-cancel_button').click(function(){
        $('#category_form .alert').remove();
        $('#category_form input[type=text]').val('');
      });
      
      $('#items_list').on('click', '.edit_item', function(){
        $('#category_form .alert').remove();
        var id = $(this).attr('data-item-num');
        $.ajax({
          type: 'POST',
          url: '/ajax/get_category/' + id,
          dataType: 'json'
        }).done(function(result){
          if($('#category_form').hasClass('in') == false){
            $('.show-hidden-panel').click();
          }
          $('.js-add_button').addClass('hide');
          $('.js-edit_button').removeClass('hide');
          if($('#item_id').length > 0){
            $('#item_id').val(id);
          }else{
            $('#category_form form').prepend('<input type="hidden" id="item_id" value="' + result.id + '" />');
          }
          $('.item_title').val(result.title);
          $('.item_description').val(result.description);
        }); 
      });
      
      $('#items_list').on('click', '.delete_item', function(){
        var el = $('.categories_wrap');
        var id = $(this).attr('data-item-num');
        ajaxStart(el);
        $.ajax({
          type: 'POST',
          url: '/ajax/delete_category/' + id,
          dataType: 'json'
        }).done(function(result){
          ajaxEnd(el, result);
          if(result.status == 'done'){
            $('#items_list').load('/ajax/get_all_categories');
          }
        }); 
      });
      
    });
    </script>
  </form>
</div>
<button class="btn btn-primary show-hidden-panel" type="button" data-toggle="collapse" data-target="#category_form">Добавить категорию</button>
<hr />
<div class="categories_wrap">
  <h4>Список категорий</h4>
  <div id="items_list">
    <?php echo $list; ?>
  </div>
</div>