<header class="header">
  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="<?php echo URL::site('/admin/'); ?>">Portfolio</a>
        <div class="nav-collapse">
          <ul class="nav">
            <li><a href="<?php echo URL::site('/admin/works'); ?>">Работы</a></li>
            <li><a href="<?php echo URL::site('/admin/categories'); ?>">Категории</a></li>
            <li><a href="<?php echo URL::site('/admin/orders/'); ?>">Заявки</a></li>
            <li><a href="<?php echo URL::site('/admin/static'); ?>">Статические страницы</a></li>
            <li><a href="<?php echo URL::site('/admin/stats'); ?>">Статистика</a></li>
            <li><a href="<?php echo URL::site('/admin/settings'); ?>">Настройки</a></li>
          </ul>
          <p class="navbar-text pull-right">
            Добро пожаловать, <?php echo $username; ?> <a href="<?php echo URL::site('/admin/logout'); ?>" class="navbar-link"> (Выйти)</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>

<section class="container-fluid box content">
  <?php echo $section; ?>
</section>

<footer class="container-fluid box footer">
  <p>Copiryght &copy; <?php echo date('Y'); ?></p>
</footer>