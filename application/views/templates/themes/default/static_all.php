<h1>Список страниц</h1>
<ul class="pages_list">
  <?php foreach($pages as $id => $page): ?>
  <li class="item"><?php echo HTML::anchor('/static/'.$page['alias'], $page['page_title']); ?></li>
  <?php endforeach ?>
</ul>