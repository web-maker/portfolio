<!DOCTYPE html>
<html>
<head>
  <title><?=$title?></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="description" content="<?echo $description; ?>" />
  <?php echo HTML::script('bootstrap/js/jquery-1.10.2.min.js')."\r\n"; ?>
  <?php echo HTML::style('bootstrap/themes/default/styles.css')."\r\n"; ?>
</head>
<body>

  <div id="wrap">

    <header id="header" class="clearfix">
      <div class="logo">
        <p><span>Alexandr Stovbur</span> web-developer</p>
      </div>
      <nav id="top_menu">
        <li><?php echo HTML::anchor('/', 'my works'); ?></li>
        <li><?php echo HTML::anchor('/static/about', 'about me'); ?></li>
        <li><?php echo HTML::anchor('/feedback', 'contacts'); ?></li>
      </nav>
    </header>
    <section id="content">
      <?php echo $content; ?>
    </section>
    
    <footer id="footer">
      <p>Copyright &copy; <?php echo date('Y'); ?></p>
    </footer>
    
  </div>
  
</body>