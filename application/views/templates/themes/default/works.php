<ul id="works" class="works clearfix"></ul>
<button class="more">Show more</button>
<script>
  var page = 1;
  
  function getWorks(page){
    $.ajax({
      type: 'POST',
      url: '/ajax/get_works/'+page
    }).done(function(result){
      if(result == 0){
        $('.more').addClass('inactive');
        return false;
      }
      $('#works').append(result);
    });
  }

  $(document).ready(function(){
    getWorks(page);
    page++;
    $('.more').click(function(){
      if($(this).hasClass('inactive')){
        return false;
      }
      getWorks(page);
      page++;
    });
  });
</script>