<h1>Обратная связь</h1>
<div class="page_inner">
  <form method="post" action="" id="feedback">
    <div class="field_item">
      <input type="text" id="title" class="input-xxlarge" placeholder="Ваше имя" name="name" value="" />
    </div>
    <div class="field_item">
      <input type="text" id="description" class="input-xxlarge" placeholder="Ваш email" name="email" value="" />
    </div>
    <div class="field_item">
      <div class="controls input-xxlarge description_textarea">
        <textarea cols="3" rows="15" class="editor" placeholder="Введите текст сообщения" name="page_body" id="page_body"></textarea>
      </div>
    </div>
    <div class="form_action">
      <button class="btn btn-success" type="button" id="submit">Отправить</button>
    </div>
  </form>
</div>