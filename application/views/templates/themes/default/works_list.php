<?php foreach($works as $key => $work): ?>
<li class="work_item">
  <a href="/works/<?php echo $work['id']; ?>">
    <span class="title"><?php echo $work['item_name']; ?></span>
    <?php if(!empty($work['preview'])) :?>
      <img src="/image/get_image/<?php echo $work['preview']; ?>/230/230" alt="<?php echo $work['preview_desc']; ?>" />
    <?php endif; ?>
  </a>
</li>
<?php endforeach; ?>