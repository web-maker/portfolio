<h1><?php echo $work['item_name']; ?></h1>
<span class="date">добавлено <?php echo $work['date']; ?></span>
<div class="page_inner">
  <?php echo $work['item_description']; ?>
</div>
<?php if(!empty($work['images'])): ?>
<h4>Скриншоты</h4>
<div class="works_images clearfix">
  <?php foreach($work['images'] as $key => $work_image): ?>
    <div class="item">
      <img src="/image/get_image/<?php echo $work_image['name'] ?>" alt="<?php echo $work_image['description'] ?>" />
    </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>