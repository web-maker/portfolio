-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 16, 2014 at 06:15 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`) VALUES
(1, 'test title', 'test description'),
(2, 'test 2', 'test desc 2'),
(3, '1', '2'),
(5, '11122', '22233'),
(6, '111', '222'),
(7, '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `group_name` varchar(128) NOT NULL DEFAULT 'site',
  `config_key` varchar(128) NOT NULL,
  `config_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`group_name`, `config_key`, `config_value`) VALUES
('site', 'title', 's:28:"This is a titles from config";'),
('site', 'description', 's:10:"This is aw";');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `work_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `description`, `work_id`) VALUES
(64, '536100db164b7', 'Изображение 2', 72),
(65, '536100fc2123a', 'Photo 3', 72),
(66, '53610103a27d3', 'Photo 4', 72),
(67, '53610fbd92ba6', '', 72),
(69, '53610feac248f', '', 74),
(70, '536111f6ae0d1', 'фыв', 76),
(71, '536111fc7e310', '123', 76),
(72, '536112174873d', '', 77),
(73, '536112195e638', '123', 77),
(74, '5361127bb0d7c', 'gggg', 78),
(75, '5361127db13eb', '', 78),
(76, '5361148d5c402', '11', 79),
(77, '536114920c1e1', '12', 79),
(78, '53735967abfc3', '', 162),
(79, '53735f0f95422', '', 164),
(80, '53735f1366ba9', 'test', 164),
(81, '5374da9c72452', '', 164);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.');

-- --------------------------------------------------------

--
-- Table structure for table `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(5, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `static`
--

CREATE TABLE IF NOT EXISTS `static` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_body` text,
  `locale` varchar(3) DEFAULT NULL,
  `origin_record_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `static`
--

INSERT INTO `static` (`id`, `title`, `meta_description`, `alias`, `page_title`, `page_body`, `locale`, `origin_record_id`) VALUES
(72, 'asdasdasd', '', '', '', '', NULL, 0),
(73, '132', '', '', '', '', NULL, 0),
(74, 'qwe', '1', '2', '3', '<p>123</p>', NULL, 0),
(75, 'sdfфы', '1', '2', '3', '<p>4</p>', NULL, 0),
(76, 'asda2', 'asdas', 'dasda', 'asdasd', '<p>фыфыфывфывыв</p>', NULL, 0),
(78, 'asdфыфывфывzasd', 'asdsas', 'asdasd', 'asdasd', '<p>asdфывфыв</p>', NULL, 0),
(79, 'Meta-title', 'This is meta-description', 'about', 'About me', '<p>This is a page about me</p>', NULL, 0),
(80, '1', '2', '3', '4', '<p>5</p>', NULL, 0),
(83, '1', '2', 'asd', 'asd', '<p>asdad</p>', NULL, 0),
(84, '23', 'qw', '2asd', 'asdad', '', NULL, 0),
(85, 'Contacts', 'Contacts', 'contacts', 'Contacts', '<p>This is contacts</p>', NULL, 0),
(86, 'йцуйц', 'йцуйцу', 'sdfsdfs', 'йуцйцу', '<p>sadasdasd</p>', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stats`
--

CREATE TABLE IF NOT EXISTS `stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(20) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Statistics table' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `stats`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `logins`, `last_login`) VALUES
(5, 'test@admin.com', 'admin', '9c1fba1cef4888377829f5d3ba540f55e4a9c6ee85485d81481c1bc1dee99118', 65, 1400068657);

-- --------------------------------------------------------

--
-- Table structure for table `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_tokens`
--


-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE IF NOT EXISTS `works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` text,
  `category` int(11) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Works table' AUTO_INCREMENT=182 ;

--
-- Dumping data for table `works`
--

INSERT INTO `works` (`id`, `title`, `meta_description`, `item_name`, `item_description`, `category`, `preview`, `date`) VALUES
(72, '1', '2', '3', '<p>4</p>', NULL, '53610fbd92ba6', '30.04.2014'),
(73, '1', '2', '34', '<p>4</p>', NULL, NULL, '30.04.2014'),
(75, 'ываыва', 'ываыва', 'ываыва', '<p>ываыв</p>', NULL, NULL, '30.04.2014'),
(157, '1', '2', '3', '<p>4</p>', NULL, '', '06.05.2014'),
(158, '1', '2', '3', '<p>4</p>', NULL, '', '06.05.2014'),
(159, '1', '2', '3', '<p>4</p>', NULL, '', '06.05.2014'),
(160, '1', '2', '3', '<p>4</p>', NULL, '', '14.05.2014'),
(161, '1', '2', '3', '<p>4</p>', NULL, '', '14.05.2014'),
(164, 'Test meta title', 'Test meta description', 'Test title', '<p>Test description</p>', 1, '53735f0f95422', '14.05.2014');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `roles_users`
--
ALTER TABLE `roles_users`
  ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
